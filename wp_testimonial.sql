-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 09, 2017 at 05:08 
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpresstraining_task3`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_testimonial`
--

CREATE TABLE `wp_testimonial` (
  `testimonial_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `testimonial_name` varchar(100) NOT NULL,
  `testimonial_email` varchar(50) NOT NULL,
  `testimonial_phone` varchar(50) NOT NULL,
  `testimonial_testimonial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_testimonial`
--

INSERT INTO `wp_testimonial` (`testimonial_id`, `blog_id`, `testimonial_name`, `testimonial_email`, `testimonial_phone`, `testimonial_testimonial`) VALUES
(1, 2, 'Testimonial Multi Site 2', 'testimonialmultisite2@gmail.com', '432524', 'tresgfsdgds'),
(2, 2, 'Testimonial Data Multi Site 2', 'testimonialdatamultisite2@gmail.com', '6546', 'hjfdgbgcx'),
(3, 3, 'Testimonial Data Multi Site 3', 'testimonialmulti3@gmail.com', '12312', 'gfdsgds'),
(4, 3, 'Testimonial Multi Site 3', 'testimonialdatamulti3@gmail.com', '654634', 'ydsghds'),
(5, 4, 'Test Testimonial 4', 'testvalidation@gmail.com', '4324324', 'dhsfhdf'),
(6, 4, 'Test Data Testimonial 4', 'testdatavalidation@gmail.com', '787654', 'gfdsgdfbgds');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_testimonial`
--
ALTER TABLE `wp_testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_testimonial`
--
ALTER TABLE `wp_testimonial`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
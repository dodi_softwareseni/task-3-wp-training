<?php
/*
Plugin Name: Testimonial
Plugin URI: http://example.com
Description: Simple non-bloated WordPress Contact Form
Version: 1.0
Author: Agbonghama Collins
Author URI: http://w3guy.com
*/


function html_form_code() {
	echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
	echo '<p>';
	echo 'Name (required) <br/>';
	echo '<input type="text" name="user_testimonial_name" pattern="[a-zA-Z0-9 ]+" value="' . ( isset( $_POST["user_testimonial_name"] ) ? esc_attr( $_POST["user_testimonial_name"] ) : '' ) . '" size="40" />';
	echo '</p>';
	echo '<p>';

	echo 'Email (required) <br/>';
	echo '<input type="email" name="user_testimonial_email" value="' . ( isset( $_POST["user_testimonial_email"] ) ? esc_attr( $_POST["user_testimonial_email"] ) : '' ) . '" size="40" />';
	echo '</p>';
	echo '<p>';

	echo 'Phone (required) <br/>';
	echo '<input type="text" name="user_testimonial_phone"  value="' . ( isset( $_POST["user_testimonial_phone"] ) ? esc_attr( $_POST["user_testimonial_phone"] ) : '' ) . '" size="40" />';
	echo '</p>';
	echo '<p>';

	echo 'Testimonial (required) <br/>';
	echo '<textarea rows="10" cols="35" name="user_testimonial_testimonial">' . ( isset( $_POST["user_testimonial_testimonial"] ) ? esc_attr( $_POST["user_testimonial_testimonial"] ) : '' ) . '</textarea>';
	echo '</p>';

	echo '<p><input type="submit" name="cf-submitted" value="Send"></p>';
	echo '</form>';
}

function insert_data() {
	global $wpdb;

	if ( isset( $_POST['cf-submitted'] ) ) 
	{
		$name    = sanitize_text_field($_POST["user_testimonial_name"]);
		$email   = sanitize_email($_POST["user_testimonial_email"]);
		$phone = sanitize_text_field($_POST["user_testimonial_phone"]);
		$testimonial = esc_textarea($_POST["user_testimonial_testimonial"]);

		if($_POST['user_testimonial_name'] != '' || $_POST['email'] != '' ||$_POST['phone'] != '' ||$_POST['user_testimonial_testimonial'] != '')
		{
			global $blog_id;

			$wpdb->insert('wp_testimonial', array( 
				'blog_id' => $blog_id,
				'testimonial_name' => $name, 
				'testimonial_email' => $email,
				'testimonial_phone' => $phone, 
				'testimonial_testimonial' => $testimonial
				), 
				array('%s', '%s', '%s', '%s') 
			);
			echo "<p> Success! </p>";
		}
		else
		{
			echo "<p> Testimonial Data must be filled!</p>";
		}

	}
}

function cf_shortcode() {
	ob_start();
	insert_data();
	html_form_code();

	return ob_get_clean();
}

add_shortcode( 'testimonial', 'cf_shortcode' );

add_action( 'admin_menu', 'my_admin_menu' );

function my_admin_menu() {
	add_menu_page( 'Testimonial Data', 'Testimonial Data', 'manage_options', 'dodi_testimoni', 'myplugin_admin_page', 'dashicons-tickets', 6  );
}

function myplugin_admin_page($blog_id){
	$blog_id = get_current_blog_id();
	if(isset($_GET['tid'])){
		delete_data($_GET['tid']);
	} 
	?>
		<div class="wrap">
			<h2>Testimonial Data</h2>
			<br><br>
			<table border="2">
			    <tr>
			    	<th>No</th>
			        <th>Name</th>
			        <th>Email</th>
			        <th>Phone</th>
			        <th>Testimonial</th>
			        <th>Action</th>
			    </tr>
			    <tr>
			        <?php
			        	$no=1;
					    global $wpdb;
					    global $blog_id;
					    $result = $wpdb->get_results ( "SELECT * FROM wp_testimonial WHERE blog_id = $blog_id " );
					    foreach ( $result as $print ){
						    ?>
							    <tr>
							    	<td><?php echo $no++;?></td>
								    <td><?php echo $print->testimonial_name;?></td>
								    <td><?php echo $print->testimonial_email;?></td>
								    <td><?php echo $print->testimonial_phone;?></td>
								    <td><?php echo $print->testimonial_testimonial;?></td>
								    <td>
								    <a href="<?php echo admin_url('admin.php?page=dodi_testimoni&tid=' 
								    .

								    $print->testimonial_id
								    ); ?>">Delete</a>
								    
								    </td> 
							    </tr>
							<?php 
						}
					  ?>      
			    </tr>
			</table>
		</div>
	<?php
}

// function delete_data($id, $blog_id) { 
//    global $wpdb;
//    $wpdb->query("
//        DELETE FROM wp_testimonial WHERE post=$postId"
//    ;);
// }

function delete_data($id){
    global $wpdb;
    $blog_id = get_current_blog_id();
	$wpdb->query( 
	 	$wpdb->prepare( 
	    "
	        DELETE FROM wp_testimonial
	     	WHERE testimonial_id = %d AND blog_id = %d",
	        $id, $blog_id
	    )
	);
}

class RandomPostWidget extends WP_Widget
{
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'RandomPostWidget', 
    		'description' => 'Displays a random post'
		);
		parent::__construct( 'RandomPostWidget', 'My Widget', $widget_ops );
	}
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
	?>
	  <p>
	  <label for="<?php echo $this->get_field_id('title'); ?>">Title: 
	  	<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
	  </label>
	  </p>
	<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    // WIDGET CODE GOES HERE
  	?>
		<div class="wrap">
			<?php
			    global $wpdb;
			    global $blog_id;
			    $result = $wpdb->get_results ( "SELECT * FROM wp_testimonial WHERE blog_id = $blog_id ORDER BY RAND()
LIMIT 1" );
			    foreach ( $result as $print ){
				    ?>
					    <ul>
						  	<li>Name 		: <?php echo $print->testimonial_name;?></li>
						    <li>Email 		: <?php echo $print->testimonial_email;?></li>
						    <li>Phone 		: <?php echo $print->testimonial_phone;?></li>
						    <li>Testimonial : <?php echo $print->testimonial_testimonial;?></li>
						  </ul>
					<?php 
				}
			  ?>   
		</div>
	<?php
 
    echo $after_widget;
  }
 
}

// register Foo_Widget widget
function register_foo_widget() {
    register_widget( 'RandomPostWidget' );
}
add_action( 'widgets_init', 'register_foo_widget' );

?>
